const express = require('express');
const routers = express.Router();

routers.get('/',(req,res)=>{
    res.render('pages/teaching',{matiere:''});
});

routers.get('/javascript',(req,res)=>{
    res.render('pages/teaching',{matiere:'Javascript'});
});

routers.get('/php',(req,res)=>{
    res.render('pages/teaching',{matiere:'Javascript'});
});

routers.get('/node',(req,res)=>{
    res.render('pages/teaching',{matiere:'NodeJS'});
});

routers.get('/node/express',(req,res)=>{
    res.render('pages/teaching',{matiere:'Express'});
});

module.exports = routers;
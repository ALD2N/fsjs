const express =require('express');
const app = express();

const path = require('path');
app.use(express.static(path.join(__dirname,'public')));


// définition du view engine
app.set('view engine', 'ejs'); // npm install --save ejs
app.set('views', path.join(__dirname, 'views'));

// layout
const expressLayouts = require('express-ejs-layouts');
//ajout du middleware
app.use(expressLayouts);
//définition du layout par défaut
app.set('layout', '../views/layouts/layout');


const homeRouter = require('./routes/homeRouter');
app.use('/jokes', homeRouter)
app.use('*', homeRouter)




module.exports = app;
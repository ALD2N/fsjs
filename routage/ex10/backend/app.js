const express = require('express');
const app = express();

const path = require('path');

app.use(express.static(path.join(__dirname,'public')));

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

const expressLayouts = require('express-ejs-layouts');
app.use(expressLayouts);
app.set('layout','../views/layouts/layout')

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.get('/', (req,res) => {
    let query=req.query;
    res.render('pages/show_route_params',{query})
});

app.get('/form', (req,res) => {
    res.render('pages/form')
});


app.post('/form', (req,res,next) => {
    let data=req.body;
    res.render('pages/show_post_data',{data})
});




module.exports = app;
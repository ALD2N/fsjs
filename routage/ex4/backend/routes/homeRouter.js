const express = require('express');
const router = express.Router();

function User(nickname, sex) {
    this.nickname = nickname;
    this.sex = sex;
}

router.get('/',(req,res)=>{
    res.render('pages/home', { user: new User('Cattoen','M')}) ;
});

module.exports = router;
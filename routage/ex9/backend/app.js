const express = require('express');
const app = express();

const path = require('path');

app.use(express.static(path.join(__dirname,'public')));

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

const expressLayouts = require('express-ejs-layouts');
app.use(expressLayouts);
app.set('layout','../views/layouts/layout')

function User(nickname, sex) {
    this.nickname = nickname;
    this.sex = sex;
}

// app.use((req, res) => {
//     res.render('pages/home', { user: new User("Axel","M")}) ;
// });

app.get('/', (req,res) => {
    let query=req.query;
    res.render('pages/show_route_params',{query})
});

module.exports = app;
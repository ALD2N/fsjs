const express = require('express');
const routers = express.Router();

routers.get('/',(req,res)=>{
    res.send('<h1>Enseignement</h1>');
});

routers.get('/javascript',(req,res)=>{
    res.send('<h1>Enseignement de javascript</h1>');
});

routers.get('/php',(req,res)=>{
    res.send('<h1>Enseignement de php</h1>');
});

routers.get('/node',(req,res)=>{
    res.send('<h1>Enseignement de node</h1>');
});

routers.get('/node/express',(req,res)=>{
    res.send('<h1>Enseignement du framework Express</h1>');
});

module.exports = routers;
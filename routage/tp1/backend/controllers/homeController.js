const fs = require('fs').promises;
let jokes = undefined;
fs.readFile('data/jokes.json').then(function (e){
    jokes = JSON.parse(e);
});

module.exports.list=(req, res)=>{
	res.render('pages/list', {jokes});
};

module.exports.random=(req, res)=>{
	randomJoke = jokes[Math.floor(jokes.length*Math.random())]['joke'];
	res.render('pages/random', {randomJoke});
};
const express = require('express');
const router = express.Router();

const homeController = require('../controllers/homeController');
router.get('/list', homeController.list);
router.get('/random', homeController.random);
router.get('*', homeController.list);
module.exports = router;
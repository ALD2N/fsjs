module.exports.home = (req, res)=>{
    res.send('<h1>Enseignement</h1>');
}

module.exports.javascript = (req, res)=>{
    res.send('<h1>Enseignement de javascript</h1>');
}

module.exports.php = (req, res)=>{
    res.send('<h1>Enseignement de php</h1>');
}

module.exports.node = (req, res)=>{
    res.send('<h1>Enseignement de node</h1>');
}

module.exports.nodeExpress = (req, res)=>{
    res.send('<h1>Enseignement du framework Express</h1>');
}
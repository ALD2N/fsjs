const express = require('express');
const app = express();

const path = require('path');

app.use(express.static(path.join(__dirname,'public')));

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

const expressLayouts = require('express-ejs-layouts');
app.use(expressLayouts);
app.set('layout','../views/layouts/layout')

function User(nickname, sex) {
    this.nickname = nickname;
    this.sex = sex;
}


const homeRouteur = require('./routes/homeRouter');
app.use('/',homeRouteur);

const aboutRouter = require('./routes/aboutRouter');
app.use('/about',aboutRouter);

const researchRouter = require('./routes/researchRouter');
app.use('/research',researchRouter)

const teachingRouter = require('./routes/teachingRouter');
app.use('/teaching',teachingRouter)


app.get('/*',(req,res) =>{
    res.send('<p>Page inconnue</p>');
});

module.exports = app;
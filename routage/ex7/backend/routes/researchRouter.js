const express = require('express');
const router = express.Router();

const researchRouter = require('../controllers/researchController');
router.get('/', researchRouter.home);

module.exports = router;
const express = require('express');
const routers = express.Router();

const teachingController = require('../controllers/teachingController');

routers.get('/', teachingController.home);
routers.get('/javascript', teachingController.javascript);
routers.get('/php', teachingController.php);
routers.get('/node', teachingController.node);
routers.get('/nodeExpress', teachingController.nodeExpress);

module.exports = routers;